package org.selenide.examples.user;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Selenide.*;

public class TestLogin {


    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");

    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }


    @Test
    public void connection(){
        //$("button[class='accept']").click();
        //$("input[type='text']").click();

        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
        $("i[class='fa-solid fa-right-from-bracket']").click();
    }

    @Test
    public void connectionPWNOK(){
        //System.out.println(title());
        //$("button[class='accept']").click();
        //$("#app > section > section > main > div > div:nth-child(2) > div.el-card.is-hover-shadow.box-card > div.el-card__body > div.popup-error > div.loginFailed > button.accept").click();
        $("input[type='text']").click();
        //WebElement a = $("title");
        $("input[type='text']").sendKeys("cleriell");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("teste");
        $("button[class$='el-button--large']").click();
        $("p").shouldHave(exactText("Connexion échouée"));
        //assertEquals("", title());
    }

    @Test
    public void connectionUserNOK(){
        //System.out.println(title());
        //$("button[class='accept']").click();
        //$("#app > section > section > main > div > div:nth-child(2) > div.el-card.is-hover-shadow.box-card > div.el-card__body > div.popup-error > div.loginFailed > button.accept").click();
        $("input[type='text']").click();
        //WebElement a = $("title");
        $("input[type='text']").sendKeys("clerielel");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
        $("p").shouldHave(exactText("Connexion échouée"));
        //assertEquals("", title());

    }

}
