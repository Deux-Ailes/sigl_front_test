package org.selenide.examples.apprentice;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$$;

public class CreatePeriodNotesTest {
    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToApprentice(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Apprenti")).click();
    }

    public void goTo(String nomDivision){
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(2) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText(nomDivision)).click();
    }

    @Test
    public void creationNoteOK(){
        navigationToApprentice();
        goTo("Notes");

        // Partie popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie semestre
        $("div[class='el-input__wrapper']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();
        // Partie sélection date
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(3) > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(4) > td:nth-of-type(5) > div").click();
        // Partie texte
        $("textarea[class^='el-textarea']").sendKeys("Ceci est un paragraphe rempli par un programme de test. Merci de ne pas y faire attention.");
        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
    }

    @Test
    public void creationNoteNoSemester(){
        navigationToApprentice();
        goTo("Notes");

        // Partie popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

         // Partie sélection date
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(3) > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(4) > td:nth-of-type(5) > div").click();
        // Partie texte
        $("textarea[class^='el-textarea']").sendKeys("Ceci est un paragraphe rempli par un programme de test. Merci de ne pas y faire attention.");
        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void creationNoteNoDate(){
        navigationToApprentice();
        goTo("Notes");

        // Partie popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie semestre
        $("div[class='el-input__wrapper']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();
        // Partie texte
        $("textarea[class^='el-textarea']").sendKeys("Ceci est un paragraphe rempli par un programme de test. Merci de ne pas y faire attention.");
        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        $("div[class='el-dialog']").should(exist);
    }

    @Test
    public void deleteFirstNote(){
        creationNoteOK();
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div > div:nth-of-type(1) > div:nth-of-type(1) > div").click();
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > button:nth-of-type(2)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
    }


}
