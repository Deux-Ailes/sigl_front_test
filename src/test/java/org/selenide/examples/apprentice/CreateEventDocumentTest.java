package org.selenide.examples.apprentice;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$$;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateEventDocumentTest {

    String texteLibelle = "testLibelle";
    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToApprentice(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(exactText("Apprenti")).click();
    }

    @Test
    public void createEventDocOK() {
        navigationToApprentice();

        // Sélection du sous-menu
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(2) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Livrables")).click();

        // Accès au menu d'ajout des livrables
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Bouton ajouter
        $("div[class='el-dialog']").should(exist);

        // Partie type de document
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > div > div").click();
        Selenide.Wait();
        ElementsCollection listeDocumentsTypes = $$("li[class='el-select-dropdown__item']");
        listeDocumentsTypes.first().click();
        //$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1) > span").should(exist).click();
        // Partie libellé
        $("input[placeholder='Libellé']").should(exist).sendKeys(texteLibelle);
        // Partie drop de document
        File file = new File("src/test/java/org/selenide/examples/apprentice/FSS9.pdf");
        $("input[class$='input']").should(exist).uploadFile(file); // Transmission du fichier à l'input
        //assertTrue($("span[class$='item-file-name']").getText().contains(file.getName()));
        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").should(exist).click();
        //Selenide.sleep(2000);
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
        // Consultation table pour seconde validation
    }

    public boolean estPresentDansTable(String libelle){

        return false;
    }
}
