package org.selenide.examples.admin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

/**
 * Classe permettant de tester l'ajout de livrables
 */
public class AjoutLivrableTest {

    /**
     * Set up de la config
     */
    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("allure", new AllureSelenide()
        .screenshots(true)
                .savePageSource(true));
    }

    /**
     * Deconnexion.
     */
    @AfterEach
    public void deconnexion(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    /**
     * Connexion.
     */
    @BeforeEach
    public void connexion(){
        open("http://alternapp.fr/");
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
        Selenide.Wait();
    }

    @Test
    public void navigationToAdmin(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Administrateur")).click();
        //listeRoles.get(1).should(exist).click();
    }

    public void goTo(String nomDivision){
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(1) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText(nomDivision)).click();
    }

    /**
     * Creation d'un livrable OK
     */
    @Test
    public void creationLivrableNoToggle(){
        //navigationToAdmin();
        goTo("Ajout de livrables");

        // Partie Popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie livrable
        $("input[placeholder='livrable']").sendKeys("testLivrable");

        // Partie libellé
        $("input[placeholder='Libellé']").sendKeys("testLibelle");

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void creationLivrableNoLabel(){
        //navigationToAdmin();
        goTo("Ajout de livrables");

        // Partie Popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie livrable
        $("input[placeholder='livrable']").sendKeys("testLivrable");

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void creationLivrableNoLivrable(){
        //navigationToAdmin();
        goTo("Ajout de livrables");

        // Partie Popup
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libellé
        $("input[placeholder='Libellé']").sendKeys("testLibelle");

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }
}
