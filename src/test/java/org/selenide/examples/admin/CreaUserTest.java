package org.selenide.examples.admin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CreaUserTest {

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(true)
                .savePageSource(true)
        ); }


    @AfterEach
    public void deconnexion(){
        Selenide.clearBrowserCookies();
        $("i[class='fa-solid fa-right-from-bracket']").click();
    }

  public void goTo(String nomDivision){
    ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(1) > ul > li"); // Sélection dans le menu à gauche
    listeBoutons.find(partialText(nomDivision)).click();
  }
    @BeforeEach
    public void connexion(){

        open("http://alternapp.fr/");
        //$("button[class='accept']").click();
        //$("input[type='text']").click();

        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
        //assertNotEquals("http://194.195.243.150:8080/front/login", Selenide.title());
        Selenide.Wait();
    }

    /**
     * Creation d'un utilisateur OK
     */
    @Test
    public void creationUserOK(){
        goTo("Création");
        ElementsCollection listeChamps = $$("input[type='text']");
        String username = getAlphaNumericString(6);
        listeChamps.get(0).sendKeys("prenom");
        listeChamps.get(1).sendKeys("nom");
        listeChamps.get(2).sendKeys(username+"@gmail.com");
        listeChamps.get(3).click();
        ElementsCollection listeRoles = $$("li[class='el-select-dropdown__item']");
        listeRoles.get(6).click();
        listeChamps.get(4).sendKeys(username);
        listeChamps.get(0).sendKeys("test");
        $("#app > section > section > main > div > div.el-col.el-col-12.is-guttered > div.el-card.is-hover-shadow.box-card > div.el-card__body > div:nth-child(2) > button:nth-child(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("AxiosError"));
    }

    /**
     * Creation d'un utilisateur avec un champ manquant
     */
    @Test
    public void creationUserMissingField(){
        goTo("Création");
        ElementsCollection listeChamps = $$("input[type='text']");
        String username = getAlphaNumericString(6);
        listeChamps.get(0).sendKeys("prenom");
        //listeChamps.get(1).sendKeys("nom");
        //listeChamps.get(2).sendKeys(username+"@gmail.com");
        listeChamps.get(3).click();
        ElementsCollection listeRoles = $$("li[class='el-select-dropdown__item']");
        listeRoles.get(6).click();
        listeChamps.get(4).sendKeys(username);
        listeChamps.get(0).sendKeys("test");
        $("#app > section > section > main > div > div.el-col.el-col-12.is-guttered > div.el-card.is-hover-shadow.box-card > div.el-card__body > div:nth-child(2) > button:nth-child(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("AxiosError"));
    }

    /**
     * Creation d'un utilisateur avec tous les champs vides
     */
    @Test
    public void creationUserEmptyFields(){
        goTo("Création");
        $("#app > section > section > main > div > div.el-col.el-col-12.is-guttered > div.el-card.is-hover-shadow.box-card > div.el-card__body > div:nth-child(2) > button:nth-child(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("AxiosError"));
    }

    /**
     * Génération d'une string aléatoire de taille n
     */
    static String getAlphaNumericString(int n)
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

}
