package org.selenide.examples.admin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class ChangePasswordTest {
  @BeforeEach
  public void setUpAll() {
    Configuration.browserSize = "1280x800";
    SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
      .screenshots(false)
      .savePageSource(true)
    );
    open("http://alternapp.fr");
    connection();
    navigationToAdmin();
  }

  @AfterEach
  public void closeAll(){
    clearBrowserCookies();
    clearBrowserLocalStorage();
    closeWindow();
  }

  public void connection(){
    $("input[type='text']").sendKeys("rischcle");
    $("input[type='password']").click();
    $("input[type='password']").sendKeys("test");
    $("button[class$='el-button--large']").click();
  }

  @Test
  public void navigationToAdmin() {
    $("div[class='el-dropdown']").click();
    ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
    listeRoles.find(exactText("Administrateur")).click();
  }


  @Test
  public void changePasswordOK(){
    ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(1) > ul > li"); // Sélection dans le menu à gauche
    listeBoutons.find(partialText("mdp")).should(exist).click();

    // Partie utilisateur
    $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div > div > div > div > input").should(exist).click();
    $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(3)").click(); // Sélection de Corentin Alléard

    // Partie mdp
    $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div > input").sendKeys("test");

    // Validation
    $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(3) > button:nth-of-type(1)").click();
    SelenideElement se = $("div[role='alert']").should(exist);
    se.shouldNotHave(partialText("Error"));
  }

  @Test
  public void changePasswordNOK(){
    ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(1) > ul > li"); // Sélection dans le menu à gauche
    listeBoutons.find(partialText("mdp")).should(exist).click();

    // Partie utilisateur
    $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div > div > div > div > input").should(exist).click();
    $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(3)").click(); // Sélection de Corentin Alléard

    // Pas de changement de mot de passe
    //$("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div > input").sendKeys("test");

    // Validation
    $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(3) > button:nth-of-type(1)").click();
    SelenideElement se = $("div[role='alert']").should(exist);
    se.shouldHave(partialText("Error"));
  }
}
