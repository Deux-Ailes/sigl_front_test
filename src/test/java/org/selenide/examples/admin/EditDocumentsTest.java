package org.selenide.examples.admin;

import com.codeborne.selenide.*;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.closeWindow;
import static org.junit.jupiter.api.Assertions.*;

public class EditDocumentsTest {

    String nomDoc = "testXX";

    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();

    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();

        //$("i[class='fa-solid fa-right-from-bracket']").click();
    }

    /**
     * Acces à la popup de création
     */
    public void accessDocumentCreationPopUp(){
        $("#app > section > aside > div > section > main > div:nth-child(1) > ul > li:nth-child(5)").click(); // Sélection dans le menu à gauche du "Documents"
        $("#app > section > section > main > div.documentAdminListPage > div:nth-child(2) > div.el-col.el-col-5 > button > span").click(); // Clic sur le bouton "Ajouter
    }

    /**
     * Création d'un document avec tous les champs remplis
     */
    @Test
    public void createDocumentOK(){
        accessDocumentCreationPopUp();
        $("div[class='el-dialog']").should(exist);
        $("div[class$='el-input--suffix'] div[class$='wrapper']").should(exist).click(); // Choix du type de document (promotion)
        ElementsCollection listeProm = $$("li[class='el-select-dropdown__item']");
        listeProm.get(0).click(); // Sélection du premier élément dans les promos

        $("div[class$='el-input--default'] div[class$='wrapper']").find("input").sendKeys(nomDoc); // Ajout du nom du libelle

        File file = new File("src/test/java/org/selenide/examples/admin/E4aDC.pdf");
        $("input[class$='input']").should(exist).uploadFile(file); // Transmission du fichier à l'input
        Selenide.Wait();

        // Validation
        $("span[class='dialog-footer'] button[style='--el-button-bg-color:#8ECAE6; --el-button-text-color:var(--el-color-black); --el-button-border-color:#8ECAE6; --el-button-hover-bg-color:rgb(176, 218, 238); --el-button-hover-text-color:var(--el-color-black); --el-button-hover-border-color:rgb(176, 218, 238); --el-button-active-bg-color:rgb(118, 166, 188); --el-button-active-border-color:rgb(118, 166, 188);']").click();

        // Verification de la requête
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Création réussie du document"));
        // Vérification présence élément
        Selenide.Wait();
        assertTrue(verificationPresenceDocument(nomDoc));
    }

    /**
     * Verification de la présence d'un document dans la table en fonction de son nom
     */
    public boolean verificationPresenceDocument(String nomDoc){
        SelenideElement table = $("tbody");
        ElementsCollection roww = $$("tbody > tr");
        if(table.has(partialText(nomDoc))) return true;
        for(SelenideElement row : roww){
           if(row.has(partialText(nomDoc))) return true;
        }
        return false;
    }

    /**
     * Délétion d'un document après en avoir créé un
     */
    @Test
    public void deleteDocument(){
        createDocumentOK();
        //Selenide.refresh();
        //Selenide.sleep(1000); // Pour refresh la page
        SelenideElement table = $("tbody");
        ElementsCollection roww = $$("tbody > tr");

        for(SelenideElement row : roww){
            if(row.getText().contains(nomDoc)){ // on trouve la bonne ligne
                SelenideElement boutonDelete = row.find("div > button:nth-of-type(2)");
                boutonDelete.click();
                Selenide.Wait();
                $("div[class='el-dialog']").should(exist);
                $(By.xpath("/html/body/div[1]/section/section/main/div[3]/div/div/footer/span/button[2]/span")).should(exist).click();
                SelenideElement se = $("div[role='alert']").should(exist);
                se.shouldHave(partialText("Suppression réussie"));
                break;
            }
        }
    }


}
