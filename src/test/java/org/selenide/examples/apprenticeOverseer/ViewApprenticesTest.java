package org.selenide.examples.apprenticeOverseer;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ViewApprenticesTest {

    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    /**
     * Navigation à la page Maitre Apprentissage
     */
    @Test
    public void navigationToApprenticeOverser(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Maitre")).should(exist).click();
        //open("http://alternapp.fr/apprenticeOverseer");
    }

    /**
     * Vérification présence d'une table
     */
    @Test
    public void checkApprentices(){
        navigationToApprenticeOverser();
      //ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(5) > ul > li"); // Sélection dans le menu à gauche
      //listeBoutons.find(partialText("Apprentis")).click();
      open("http://alternapp.fr/apprenticeOverseer/apprentices");
      //$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(5) > ul > li:nth-of-type(1)").click();
      $("table[class='el-table__body']").should(exist); // Si le tableau n'existe pas alors problème
    }


}
