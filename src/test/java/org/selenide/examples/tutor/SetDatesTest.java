package org.selenide.examples.tutor;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;

public class SetDatesTest {
  @BeforeEach
  public void setUpAll() {
    Configuration.browserSize = "1280x800";
    SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    open("http://alternapp.fr");
    connection();
  }

  @AfterEach
  public void closeAll(){
    clearBrowserCookies();
    clearBrowserLocalStorage();
    closeWindow();
  }

  public void connection(){
    $("input[type='text']").sendKeys("rischcle");
    $("input[type='password']").click();
    $("input[type='password']").sendKeys("test");
    $("button[class$='el-button--large']").click();
  }

  @Test
  public void navigateToTutor(){
    $("div[class='el-dropdown']").click();
    ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
    listeRoles.find(partialText("Tuteur")).click();
  }

  public void goTo(String nomDivision){
      ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(6) > ul > li"); // Sélection dans le menu à gauche
      listeBoutons.find(partialText(nomDivision)).click();
  }

  @Test
  public void setDatesOK(){
    navigateToTutor();
    goTo("Disponibilités");

    // Partie popup
    $("div[class$='is-justify-space-between'] button[class='el-button el-button--large']").click();
    $("div[class='el-dialog']").should(exist);

    // Complétion des dates
    $("div[class$='content']").click(); // Ouverture du calendrier
    $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(3) > div").click(); // Date début calendrier gauche
    $("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(4) > td:nth-of-type(4) > div").click(); // Date fin

    // Partie validation
    $("span[class='dialog-footer'] button[style*='ECAE']").click();
    SelenideElement se = $("div[role='alert']").should(exist);
    se.shouldNotHave(partialText("Error"));
  }

  @Test
  public void erasePeriodEarliest(){
    setDatesOK(); // Pour éviter de supprimer une bonne date

    // Partie popup
    $("button[class='el-button el-button--danger el-button--large']").click();
    $("div[class='el-dialog']").should(exist);

    // Suppression
    $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(3) > div > div > div > div > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div > div:nth-of-type(1) > div > table > tbody > tr:nth-of-type(1) > td:nth-of-type(3) > div > button").click();
    SelenideElement se = $("div[role='alert']").should(exist);
    se.shouldNotHave(partialText("Error"));
  }

}
