package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;

public class RegisterCompanyTest {
    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToCoordinator(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Coordinateur")).click();
    }

    @Test
    public void companyCreationOK(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
        String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
        // Remplissage des champs
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"+ siretNumber.substring(10,13)); // Nom entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        //se.shouldNotHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoSiret(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
      String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
      // Remplissage des champs
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"); // Nom entreprise
      //$("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

      // Validation
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
      SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoName(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
      String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
      // Remplissage des champs
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

      // Validation
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
      SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoPostCode(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
      String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
      // Remplissage des champs
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"); // Nom entreprise
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

      // Validation
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
      SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoRPName(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();

        String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
        // Remplissage des champs
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"); // Nom entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoRPSurname(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
      String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
      // Remplissage des champs
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"); // Nom entreprise
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(6) > div > div > div > input").should(exist).sendKeys("test@test.fr");

      // Validation
      $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
      SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void companyCreationNoRPEmail(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Entreprises")).click();
      String siretNumber = Long.toString(System.currentTimeMillis()).substring(0,13) + "3";

        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(2) > button").click(); // Ouverture du form
        // Remplissage des champs
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > input").should(exist).sendKeys("TestEntrepriseSIGL"); // Nom entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > input").should(exist).sendKeys(siretNumber); // Siret number de 14 chiffres
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > input").should(exist).sendKeys("49000"); // Siège social Code Postal
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > input").should(exist).sendKeys("Eude"); // Nom représentant
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > input").should(exist).sendKeys("EudePrenom"); // Prénom représentant

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > footer > span > div > button:nth-of-type(2)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }
}
