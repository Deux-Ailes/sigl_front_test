package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateTutorTest {


    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Tuteurs")).click();

    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToCoordinator(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Coordinateur")).click();
    }

    @Test
    public void createTutorOK(){

        // Partie popup
        $("button[class$='el-button--large']").should(exist).click();
        $("div[class='el-dialog']").should(exist);

        // Partie utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > div > div").click();
        ElementsCollection listeUser = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeUser.last().click();

        // Partie Ecole
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > div > div").click();
        ElementsCollection listeEcoles = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeEcoles.last().click();

        //Validation
        $("span[class='dialog-footer'] button[style='--el-button-bg-color:#8ECAE6; --el-button-text-color:var(--el-color-black); --el-button-border-color:#8ECAE6; --el-button-hover-bg-color:rgb(176, 218, 238); --el-button-hover-text-color:var(--el-color-black); --el-button-hover-border-color:rgb(176, 218, 238); --el-button-active-bg-color:rgb(118, 166, 188); --el-button-active-border-color:rgb(118, 166, 188);']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
        //assertTrue(verificationPresenceNom(listeUser.last().getText()));
    }

    @Test
    public void createTutorNoUser(){

        // Partie popup
        $("button[class$='el-button--large']").should(exist).click();
        $("div[class='el-dialog']").should(exist);

        // Partie utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > div > div").click();
        ElementsCollection listeUser = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");

        // Partie Ecole
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(2) > div > div > div > div > div").click();
        ElementsCollection listeEcoles = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeEcoles.last().click();

        //Validation
        $("span[class='dialog-footer'] button[style='--el-button-bg-color:#8ECAE6; --el-button-text-color:var(--el-color-black); --el-button-border-color:#8ECAE6; --el-button-hover-bg-color:rgb(176, 218, 238); --el-button-hover-text-color:var(--el-color-black); --el-button-hover-border-color:rgb(176, 218, 238); --el-button-active-bg-color:rgb(118, 166, 188); --el-button-active-border-color:rgb(118, 166, 188);']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
        //assertFalse(verificationPresenceNom(listeUser.last().getText()));
    }

    @Test
    public void createTutorNoSchool(){

        // Partie popup
        $("button[class$='el-button--large']").should(exist).click();
        $("div[class='el-dialog']").should(exist);

        // Partie utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(1) > div > div > div > div > div").click();
        ElementsCollection listeUser = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeUser.last().click();


        //Validation
        $("span[class='dialog-footer'] button[style='--el-button-bg-color:#8ECAE6; --el-button-text-color:var(--el-color-black); --el-button-border-color:#8ECAE6; --el-button-hover-bg-color:rgb(176, 218, 238); --el-button-hover-text-color:var(--el-color-black); --el-button-hover-border-color:rgb(176, 218, 238); --el-button-active-bg-color:rgb(118, 166, 188); --el-button-active-border-color:rgb(118, 166, 188);']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
        assertFalse(verificationPresenceNom(listeUser.last().getText()));

    }

    public boolean verificationPresenceNom(String nom){
        SelenideElement table = $("tbody");
        ElementsCollection roww = $$("tbody > tr");
        if(table.has(partialText(nom))) return true;
        for(SelenideElement row : roww){
            if(row.has(partialText(nom))) return true;
        }
        return false;
    }
}
