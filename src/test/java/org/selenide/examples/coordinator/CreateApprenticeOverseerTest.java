package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateApprenticeOverseerTest {

    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToCoordinator(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Coordinateur")).click();
        //listeRoles.get(1).should(exist).click();
    }

    @Test
    public void createAOOK(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("apprentissage")).click();
        $("button[class$='el-button--large']").click();
        $("div[class='el-dialog']").should(exist);

        // Partie Utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > div > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        Selenide.sleep(500);
        ElementsCollection listeUsers = $$("html > body > div:nth-of-type(2) > div:nth-of-type(5) > div > div > div:nth-of-type(1) > ul > li");
        Selenide.sleep(2000);
        listeUsers.get(3).click();
        //Partie Entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > div > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeEntreprises = $$("html > body > div:nth-of-type(2) > div:nth-of-type(6) > div > div > div:nth-of-type(1) > ul > li");
        listeEntreprises.find(partialText("string")).click();
        //Partie position
        $("input[placeholder*='position']").sendKeys("Boss");

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").should(exist).click();

        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("AxiosError"));
    }

    @Test
    public void createAONoUser(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("apprentissage")).click();

        //Partie Entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeEntreprises = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeEntreprises.find(partialText("string")).click();
        //Partie position
        $("input[placeholder*='position']").sendKeys("Boss");

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").should(exist).click();

        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("AxiosError"));
    }

    @Test
    public void createAONoCompany(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("apprentissage")).click();

        // Partie Utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        ElementsCollection listeUsers = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeUsers.get(30).click();

        //Partie position
        $("input[placeholder*='position']").sendKeys("Boss");

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").should(exist).click();

        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("AxiosError"));
    }

    @Test
    public void createAONoPosition(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("apprentissage")).click();

        // Partie Utilisateur
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        ElementsCollection listeUsers = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeUsers.get(30).click();

        //Partie Entreprise
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeEntreprises = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeEntreprises.find(partialText("string")).click();

        // Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").should(exist).click();

        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("AxiosError"));
    }
}
