package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;

public class ViewApprenticesTest {
    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToCoordinator(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Coordinateur")).click();
    }

    @Test
    public void viewApprenticesOK(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Apprentis")).click();

        // Vérif table
        $("tbody").should(exist);
        // Acces espace apprenti
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div > div > div > div > div:nth-of-type(1) > div:nth-of-type(3) > div > div:nth-of-type(1) > div > table > tbody > tr:nth-of-type(1) > td:nth-of-type(5) > div > button").should(exist).click();
        listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("Thomas")).should(exist);
    }
}
