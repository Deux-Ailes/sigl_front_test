package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CreateEventTest {
  @BeforeEach
  public void setUpAll() {
    Configuration.browserSize = "1280x800";
    SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    open("http://alternapp.fr");
    connection();
  }

  @AfterEach
  public void closeAll(){
    clearBrowserCookies();
    clearBrowserLocalStorage();
    closeWindow();
  }

  public void connection(){
    $("input[type='text']").sendKeys("rischcle");
    $("input[type='password']").click();
    $("input[type='password']").sendKeys("test");
    $("button[class$='el-button--large']").click();
  }

  @Test
  public void navigationToCoordinator(){
    $("div[class='el-dropdown']").click();
    ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
    listeRoles.find(partialText("Coordinateur")).click();
  }

  public void goTo(String nomDivision){
    ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
    listeBoutons.find(partialText(nomDivision)).click();
  }

  @Test
  public void createEventOK(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libelle
        $("input[placeholder='Libellé']").sendKeys("TestCreationEvenement");
        // Partie calendrier
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(2) > div").click(); // Première date
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(2) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(1) > div").click(); // Seconde date
        // Partie promotion
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie semestre
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(9) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie type d'event
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > div > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(10) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();

        // Validation
          $("span[class='dialog-footer'] button[style*='ECAE']").click();
          SelenideElement se = $("div[role='alert']").should(exist);
          se.shouldNotHave(partialText("Error"));
  }

    @Test
    public void createEventNoEventType(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libelle
        $("input[placeholder='Libellé']").sendKeys("TestCreationEvenement");
        // Partie calendrier
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(2) > div").click(); // Première date
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(2) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(1) > div").click(); // Seconde date
        // Partie promotion
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie semestre
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(9) > div > div > div:nth-of-type(1) > ul > li").first().click();

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }
    @Test
    public void createEventNoSemester(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libelle
        $("input[placeholder='Libellé']").sendKeys("TestCreationEvenement");
        // Partie calendrier
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(2) > div").click(); // Première date
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(2) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(1) > div").click(); // Seconde date
        // Partie promotion
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > ul > li").first().click();
         // Partie type d'event
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > div > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(10) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void createEventNoProm(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libelle
        $("input[placeholder='Libellé']").sendKeys("TestCreationEvenement");
        // Partie calendrier
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(2) > div").click(); // Première date
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(2) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(1) > div").click(); // Seconde date
         // Partie semestre
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(9) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie type d'event
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > div > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(10) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void createEventNoLibelle(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

         // Partie calendrier
        $("div[class*='el-range-editor']").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(1) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(2) > div").click(); // Première date
        $("html > body > div:nth-of-type(2) > div:nth-of-type(7) > div > div > div > div:nth-of-type(2) > table > tbody > tr:nth-of-type(3) > td:nth-of-type(1) > div").click(); // Seconde date
        // Partie promotion
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie semestre
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(9) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie type d'event
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > div > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(10) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }

    @Test
    public void createEventNoCalendar(){
        navigationToCoordinator();
        goTo("Dates clés");

        // Partie popup
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > button").click();
        $("div[class='el-dialog']").should(exist);

        // Partie libelle
        $("input[placeholder='Libellé']").sendKeys("TestCreationEvenement");
         // Partie promotion
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(3) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(8) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie semestre
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(4) > div > div > div > div > div").click();
        $$("html > body > div:nth-of-type(2) > div:nth-of-type(9) > div > div > div:nth-of-type(1) > ul > li").first().click();
        // Partie type d'event
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(2) > div > div > div > div > div > form > div:nth-of-type(5) > div > div > div > div > div").click();
        $("html > body > div:nth-of-type(2) > div:nth-of-type(10) > div > div > div:nth-of-type(1) > ul > li:nth-of-type(1)").click();

        // Validation
        $("span[class='dialog-footer'] button[style*='ECAE']").click();
        $("div[class='el-dialog']").should(exist);
        //SelenideElement se = $("div[role='alert']").should(exist);
        //se.shouldHave(partialText("Error"));
    }

    @Test
    public void deleteFirstEvent(){
        createEventOK();
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div > div > div:nth-of-type(1) > div:nth-of-type(2) > div > div:nth-of-type(1) > div > table > tbody > tr > td:nth-of-type(8) > div > button:nth-of-type(2)").click();
        $("div[class='el-dialog']").should(exist);
        $("html > body > div:nth-of-type(1) > section > section > main > div:nth-of-type(3) > div > div > footer > span > button:nth-of-type(2)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
    }
}
