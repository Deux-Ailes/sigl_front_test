package org.selenide.examples.coordinator;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.*;

public class CreateApprenticeTeamTest {

    @BeforeEach
    public void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(false)
                .savePageSource(true)
        );
        open("http://alternapp.fr");
        connection();
    }

    @AfterEach
    public void closeAll(){
        clearBrowserCookies();
        clearBrowserLocalStorage();
        closeWindow();
    }

    public void connection(){
        $("input[type='text']").sendKeys("rischcle");
        $("input[type='password']").click();
        $("input[type='password']").sendKeys("test");
        $("button[class$='el-button--large']").click();
    }

    @Test
    public void navigationToCoordinator(){
        $("div[class='el-dropdown']").click();
        ElementsCollection listeRoles = $$("li[class^='el-dropdown-menu__item']");
        listeRoles.find(partialText("Coordinateur")).click();
    }

    @Test
    public void CreateATOK(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("tutorales")).click();

        //Partie apprenti
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        ElementsCollection listeApprentis = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeApprentis.last().click();

        //Partie tp
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeTPs = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeTPs.last().click();

        //Partie ma
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div > div").should(exist).click();
        ElementsCollection listeMA = $$("html > body > div:nth-of-type(2) > div:nth-of-type(5) > div > div > div:nth-of-type(1) > ul > li");
        listeMA.last().click();

        //Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Error"));
    }

    @Test
    public void CreateATNoAp(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("tutorales")).click();

        //Partie tp
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeTPs = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeTPs.last().click();

        //Partie ma
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div > div").should(exist).click();
        ElementsCollection listeMA = $$("html > body > div:nth-of-type(2) > div:nth-of-type(5) > div > div > div:nth-of-type(1) > ul > li");
        listeMA.last().click();

        //Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").click();
        //SelenideElement se = $("div[role='alert']").should(exist);
        //se.shouldNotHave(partialText("Error"));
    }

    @Test
    public void CreateATNoTP(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("tutorales")).click();

        //Partie apprenti
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        ElementsCollection listeApprentis = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeApprentis.last().click();


        //Partie ma
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(3) > div > div > div > div").should(exist).click();
        ElementsCollection listeMA = $$("html > body > div:nth-of-type(2) > div:nth-of-type(5) > div > div > div:nth-of-type(1) > ul > li");
        listeMA.last().click();

        //Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldNotHave(partialText("Erreur"));
    }

    @Test
    public void CreateATNoAO(){
        navigationToCoordinator();
        ElementsCollection listeBoutons = $$("html > body > div:nth-of-type(1) > section > aside > div > section > main > div:nth-of-type(4) > ul > li"); // Sélection dans le menu à gauche
        listeBoutons.find(partialText("tutorales")).click();

        //Partie apprenti
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) > div > div > div > div").should(exist).click();
        ElementsCollection listeApprentis = $$("html > body > div:nth-of-type(2) > div:nth-of-type(3) > div > div > div:nth-of-type(1) > ul > li");
        listeApprentis.last().click();

        //Partie tp
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(2) > div > div > div > div").should(exist).click();
        ElementsCollection listeTPs = $$("html > body > div:nth-of-type(2) > div:nth-of-type(4) > div > div > div:nth-of-type(1) > ul > li");
        listeTPs.last().click();


        //Validation
        $("html > body > div:nth-of-type(1) > section > section > main > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > button:nth-of-type(1)").click();
        SelenideElement se = $("div[role='alert']").should(exist);
        se.shouldHave(partialText("Error"));
    }
}
